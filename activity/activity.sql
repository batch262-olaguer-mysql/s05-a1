-- MySQL S5 Activity:

-- 1. Return the customerName of the customers who are from the Philippines.
    SELECT * FROM customers WHERE country LIKE "Philippines";

-- 2. Return the contactLastName and contactFirstName of customers with name "La Rochelle Gifts".
    SELECT contactFirstName, contactLastName FROM customers WHERE customerName LIKE "La Rochelle Gifts";

-- 3. Return the product name and MSRP of the product named "The Titanic".
    SELECT productName, MSRP FROM products WHERE productName LIKE "The Titanic";

-- 4. Return the first and last name of the employee whose email is "jfirrelli@classicmodelcars.com".
    SELECT firstName, lastName FROM employees WHERE email LIKE "jfirrelli@classicmodelcars.com";

-- 5. Return the names of customers who have no registered state.
    SELECT customerName FROM customers WHERE state LIKE '%';
    -- SELECT customerName FROM customers WHERE state LIKE '%' ORDER BY customerName ASC;

-- 6. Return the first name, last name, email of the employee whose last name is Patterson and first name is Steve.
    SELECT firstName, lastName, email FROM employees WHERE lastName LIKE "Patterson" AND firstName LIKE "Steve";

-- 7. Return customer name, country, and credit limit of customers whose countries are NOT USA and whose credit limits are greater than 3000.
    SELECT customerName, country, creditLimit FROM customers WHERE country NOT LIKE "USA" AND creditLimit > 3000;

    -- for A-Z customerName sorting.
    -- SELECT customerName, country, creditLimit FROM customers WHERE country NOT LIKE "USA" AND creditLimit > 3000 ORDER BY customerName ASC;
    -- for sorting from Descending order on Credit Limit
    -- SELECT customerName, country, creditLimit FROM customers WHERE country NOT LIKE "USA" AND creditLimit > 3000 ORDER BY creditLimit DESC;


-- 8. Return the customer numbers of orders whose comments contain the string 'DHL'.
    SELECT customerNumber FROM orders WHERE comments LIKE "%DHL%";

-- 9. Return the product lines whose text description mentions the phrase 'state of the art'.
    SELECT productLine FROM productlines WHERE textDescription LIKE "%state of the art%";

-- 10. Return the countries of customers without duplication.
    SELECT DISTINCT country FROM customers;
    -- for A-Z arrangement fo countries.
    -- SELECT DISTINCT country FROM customers ORDER BY country ASC;

-- 11. Return the statuses of orders without duplication.
    SELECT DISTINCT status FROM orders;

-- 12. Return the customer names and countries of customers whose country is USA, France, or Canada.
    SELECT customerName, country FROM customers WHERE country IN ("USA", "France", "Canada");
    -- CustomerName A-Z arrangement
    -- SELECT customerName, country FROM customers WHERE country IN ("USA", "France", "Canada") ORDER BY customerName ASC;
    -- Country A-Z arrangement
    -- SELECT customerName, country FROM customers WHERE country IN ("USA", "France", "Canada") ORDER BY country ASC;


-- 13. Return the first name, last name, and office's city of employees whose offices are in Tokyo.
    SELECT firstName, lastName, offices.city FROM employees
        JOIN offices ON employees.officeCode = offices.officeCode
        WHERE offices.city LIKE "Tokyo"; 

-- 14. Return the customer names of customers who were served by the employee named "Leslie Thompson"
    SELECT customerName FROM customers
        JOIN employees ON customers.SalesRepEmployeeNumber = employees.employeeNumber
        WHERE employees.firstName LIKE "Leslie" AND employees.lastName LIKE "Thompson";
        -- A-Z CustomerName arrangement.
        -- SELECT customerName FROM customers
        -- JOIN employees ON customers.SalesRepEmployeeNumber = employees.employeeNumber
        -- WHERE employees.firstName LIKE "Leslie" AND employees.lastName LIKE "Thompson"
        -- ORDER BY customerName ASC;

-- 15. Return the product names and customer name of products ordered by "Baane Mini Imports".
    SELECT products.productName, customers.customerName FROM orders
        JOIN customers ON orders.customerNumber = customers.customerNumber
        JOIN orderdetails ON orders.orderNumber = orderdetails.orderNumber
        JOIN products ON orderdetails.productCode = products.productCode
        WHERE customers.customerName LIKE "Baane Mini Imports";

-- 16. Return the employees' first names, employees' last names, customers' names, and offices' countries of transactions whose customers and offices are in the same country.
    SELECT employees.firstName, employees.lastName, customers.customerName, offices.country FROM customers
        JOIN payments ON customers.customerNumber = payments.customerNumber
        JOIN employees ON customers.SalesRepEmployeeNumber = employees.employeeNumber
        JOIN offices ON employees.officeCode = offices.officeCode
        WHERE customers.country = offices.country;

-- 17. Return the product name and quantity in stock of products that belong to the product line "planes" with stock quantities less than 1000.
    SELECT productName, quantityInStock FROM products
        WHERE productLine LIKE "planes" AND quantityInStock < 1000;
        -- A-Z productName Arrangement
        -- SELECT productName, quantityInStock FROM products
        -- WHERE productLine LIKE "planes" AND quantityInStock < 1000
        -- ORDER BY productName ASC; 

-- 18. Show the customer's name with a phone number containing "+81".
    SELECT customerName FROM customers
        WHERE phone LIKE "%+81%";